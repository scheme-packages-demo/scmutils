# scmutils

Numerical and algebraic packages, written in MIT/GNU Scheme, apparently.  https://ocw.mit.edu/courses/earth-atmospheric-and-planetary-sciences/12-620j-classical-mechanics-a-computational-approach-fall-2008/tools/
* https://aur.archlinux.org/packages/scmutils/ (depends on MIT Scheme)

## MIT
* [Scheme Mechanics Installation for GNU/Linux or Mac OS X
  ](https://ocw.mit.edu/courses/earth-atmospheric-and-planetary-sciences/12-620j-classical-mechanics-a-computational-approach-fall-2008/tools/)
  2008 (Contains Scmutils)
* [Scheme Mechanics Installation for GNU/Linux or Mac OS X
  ](http://ocw.nur.ac.rw/OcwWeb/Earth--Atmospheric--and-Planetary-Sciences/12-620JClassical-Mechanics--A-Computational-ApproachFall2002/Tools/index.htm)
  2002 (Contains Scmutils)
* [*SCMUTILS Reference Manual*](https://groups.csail.mit.edu/mac/users/gjs/6946/refman.txt)

## Unofficial GitHub and installation tutorial
* Tipoca/[scmutils](https://github.com/Tipoca/scmutils)
* [*Running scmutils in a preinstalled MIT/GNU Scheme*
  ](https://groups.google.com/forum/#!topic/sicm/2aoWpgJadns)
* [*SICM on Mac OS X*](https://nerdwisdom.com/tag/scmutils/)
* [*Ozymandias*](https://joelgustafson.com/ozymandias/)

## In Linux distributions
* [Versions for scmutils](https://repology.org/project/scmutils/versions)
* [AUR](https://aur.archlinux.org/packages/scmutils/)

## On the Internet
* [scmutils](https://google.com/search?q=scmutils)

## Ports and derivatives
### MIT-Scheme's "application hook" mechanism
* [Application Hooks](https://www.gnu.org/software/mit-scheme/documentation/mit-scheme-ref/Application-Hooks.html)
* [Racket](https://www.raspberrypi.org/forums/viewtopic.php?t=11789)@raspberrypi.org/forums

### Scheme
#### Chez Scheme
* fedeinthemix/[chez-scmutils](https://github.com/fedeinthemix/chez-scmutils)
* [Versions for chez-scmutils](https://repology.org/project/chez-scmutils/versions)

#### Guile
* [guile-scmutils](https://www.cs.rochester.edu/~gildea/guile-scmutils/)

### Haskell
* [Is there an equivalent of MIT Scheme's scmutils for Haskell?
  ](https://www.quora.com/Is-there-an-equivalent-of-MIT-Schemes-scmutils-for-Haskell)
* chris-taylor/[Classical-Mechanics](https://github.com/chris-taylor/Classical-Mechanics) Haskell toolbox for research and teaching in classical mechanics. Includes modules for symbolic algebra and automatic differentiation.

### Clojure
* littleredcomputer/[sicmutils](https://github.com/littleredcomputer/sicmutils)
  Scmutils in Clojure  [littleredcomputer](http://blog.littleredcomputer.net)
* [*Scmutils in Clojure*](https://www.jishuwen.com/d/28i7/zh-tw) 2016 Colin Smith
* [Physics in Clojure](https://www.youtube.com/watch?v=7PoajCqNKpg)
* [*Introspecting into a function?*](https://discourse.julialang.org/t/introspecting-into-a-function/8194)
* [About the license](https://hn.svelte.dev/item/18139367)

### Julia
* [Symbolics](https://juliaobserver.com/packages/Symbolics)
